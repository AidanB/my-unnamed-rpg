using Godot;
using System;

public class torchFlames : Particles2D
{
	public Timer timer;
	public Light2D light;
	public Random rng;
	
	public override void _Ready(){
		timer = GetNode<Timer>("Timer");
		light = GetNode<Light2D>("Light2D");
		rng = new Random();
	}

	public void OnTimeout(){
		double rand = rng.NextDouble();
		float randFloat = (float)((rand * 0.1)+1.5);
		light.Energy = randFloat;
		timer.Start(randFloat/20);
	}
}
