using Godot;
using System;

public class UILayer : CanvasLayer
{
	public float healthBarLength;
	public override void _Ready(){
		GetNode<Node>("../Player/PlayerStatus").Connect("HealthChanged", this, "PlayerHealthChanged");
		GetNode<Node>("../Player/PlayerStatus").Connect("OffenseModChanged", this, "PlayerOffenseChanged");
		GetNode<Node>("../Player/PlayerStatus").Connect("DefenseModChanged", this, "PlayerDefenseChanged");
		healthBarLength = GetNode<ColorRect>("HealthBar/ColorRect").RectSize.x;
		GD.Print(healthBarLength);
	}

	public void PlayerHealthChanged(int newHealth, int maxHealth){
		float healthPercent = (float)newHealth/maxHealth;
		GetNode<ColorRect>("HealthBar/ColorRect").RectSize = new Vector2(healthBarLength * healthPercent, 14.0F);
	}

	public void PlayerOffenseChanged(float newOffenseMod){
		GetNode<Label>("OffenseMod").Text = "OM: " + Convert.ToString(newOffenseMod);
	}

	public void PlayerDefenseChanged(float newDefenseMod){
		GetNode<Label>("DefenseMod").Text = "DM: " + Convert.ToString(newDefenseMod);
	}
}
