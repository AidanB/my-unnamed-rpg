using Godot;
using System;

public abstract class State : Node
{
	[Signal]
	public delegate void finished(String nextState);
	public abstract void Enter();
	public void Exit(){
		return;
	}
	public abstract void Update(float delta);
	
	public abstract void HandleInput(InputEvent e);

	public abstract void OnAnimationFinish(String animationName);

}
