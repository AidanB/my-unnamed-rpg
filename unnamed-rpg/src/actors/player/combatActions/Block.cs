using Godot;
using System;
namespace player{
public class Block : inputState
{
	private Timer perfectBlockTimer;
	private bool  isPerfectBlock = true;

	public override void Enter(){
		perfectBlockTimer = GetNode<Timer>("Timer");
		perfectBlockTimer.WaitTime = 0.3F;
		perfectBlockTimer.Start();
		isPerfectBlock = true;
		speed = new Vector2(0.0F, 0.0F);

		GetNode<AnimationPlayer>("../../AnimationPlayer").Play("Block");
	}

	private void OnTimeout(){
		isPerfectBlock = false;
	}
	public override void WeaponHit(Area2D area){
		if(!area.HasNode("hitbox")){
			return;
		}
		if (isPerfectBlock){
			GD.Print("Perfect Block");
			GetNode<playerStatus>("../../PlayerStatus").ChangeOffenseMod(1.0F);
			GetNode<AnimationPlayer>("../../AnimationPlayer").Stop();
			EmitSignal("finished", "previous");
		}else{
			GD.Print("block");
		}
	}
}
}
