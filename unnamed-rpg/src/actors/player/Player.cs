using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
namespace player{

public class Player : KinematicBody2D
{
	static String SPRITE = "playerSprite";
	
	[Signal]
	public delegate void StateChanged();

	private Stack stateStack = new Stack();
	public inputState curState;

	private Dictionary<String, Node> stateMap;
	public override void _Ready()
	{
		stateMap = new Dictionary<String, Node>{
			{"idle", GetNode("States/Idle")},
			{"running", GetNode("States/Running")},
			{"stance", GetNode("States/Stance")},
			{"stab", GetNode("States/Stab")},
			{"block", GetNode("States/Block")},
			{"stagger", GetNode("States/Stagger")},
			{"shove", GetNode("States/Shove")},
			{"dead", GetNode("States/Dead")},
			{"slash", GetNode("States/Slash")},
			{"sweep", GetNode("States/Sweep")}
		};
		foreach(Node stateNode in GetNode<Node>("States").GetChildren()){
			stateNode.Connect("finished", this, "ChangeState");
		}
		
		stateStack.Push(GetNode("States/Idle"));
		GetNode("PlayerStatus").Connect("HealthChanged", this, "OnHealthChanged");
		curState = (inputState)stateStack.Peek();
		ChangeState("idle");
	}

	public override void _PhysicsProcess(float delta){
		curState.Update(delta);
	}
	private void ChangeState(String stateName){
		curState.Exit();

		if(stateName == "previous"){
			stateStack.Pop();
		}else if(stateName == "stance"){
			Node newState;
			if(curState.Name == "Stance"){
				newState = stateMap["running"];
			}else{
				newState = stateMap[stateName];
			}
			stateStack.Pop();
			stateStack.Push(newState);
		}else if(new List<String>{"stagger", "stab", "block", "shove", "slash", "sweep"}.Contains(stateName)){
			stateStack.Push(stateMap[stateName]);
		}else{
			Node newState = stateMap[stateName];
			stateStack.Pop();
			stateStack.Push(newState);
		}
		curState = (inputState)stateStack.Peek();

		if(stateName != "previous"){
			curState.Enter();
		}
		//this.EmitSignal("StateChanged", stateStack);
	}

	public override void _Input(InputEvent e){
		curState.HandleInput(e);
	}

	public void TakeDamage(float amount, String[] properties){
		GetNode<playerStatus>("PlayerStatus").TakeDamage(amount, properties);
	}

	public void OnHealthChanged(int health, int maxHealth){
		if(health <= 0){
			ChangeState("dead");
		}
	}
	private void OnWeaponBodyEntered(KinematicBody2D body){
		if(!body.HasNode("Status") || curState.Name == "Block"){
			return;
		}
		curState.EnemyHit(body);
	}
	private void OnWeaponAreaEntered(RID area_rid, Area2D area, int area_shape_index, int local_shape_index){
		if(!area.HasNode("hitbox")){
			return;
		}
		if(curState.Name == "Block"){
			curState.WeaponHit(area);
		}
	}
	private void OnAnimationFinished(String anim_name){
		curState.OnAnimationFinish(anim_name);
	}
}
}
