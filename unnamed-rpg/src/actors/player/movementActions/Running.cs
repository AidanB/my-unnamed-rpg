using Godot;
using System;
namespace player{
public class Running : inputState
{
	public override void Enter(){
		//GD.Print("NOW RUNNING");
		speed = new Vector2(150.0F, 1000.0F);
		SetDirectionInput();
		UpdateDirection(direction, "../../PlayerSprite");
		GetNode<AnimationPlayer>("../../AnimationPlayer").Play("Running");
	}

	public override void HandleInput(InputEvent e){
		base.HandleInput(e);
	}

	public override void Update(float delta){
		SetDirectionInput();
		if(direction.x == 0){
			EmitSignal("finished", "idle");
		}
		UpdateDirection(direction, "../../PlayerSprite");
		velocity = CalculateMoveVelocity(velocity, speed, direction);
		velocity = GetNode<KinematicBody2D>("../../../Player").MoveAndSlide(velocity, FLOOR_NORMAL);
	}
	public override void OnAnimationFinish(string animationName){
		return;
	}
}
}
