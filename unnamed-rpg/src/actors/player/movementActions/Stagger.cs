using Godot;
using System;
namespace player{
public class Stagger : State
{
    public override void Enter(){
        GetNode<AnimationPlayer>("../../AnimationPlayer").Play("Stagger");
    }
    public override void OnAnimationFinish(string animationName)
    {
        EmitSignal("finished","previous");
    }

    public override void HandleInput(InputEvent e)
    {
        return;
    }
    public override void Update(float delta)
    {
        return;
    }
}
}
