using Godot;
using System;
namespace player{
public class Stance : inputState
{
	public override void Enter(){
		speed = new Vector2(50.0F, 1000.0F);
		SetDirectionInput();
		SetAnimation();
	}

	public override void HandleInput(InputEvent e){
		if (e.IsActionPressed("block")){
				EmitSignal("finished", "block");
		}
		if (e.IsActionPressed("attack")){
			if (Input.GetActionStrength("up") > 0){
				EmitSignal("finished","slash");
			}else if(Input.GetActionStrength("down") > 0){
				EmitSignal("finished","sweep");
			}else{
				EmitSignal("finished", "stab");
			}
		}
		
		base.HandleInput(e);
	}
	
	public override void Update(float delta){
		SetDirectionInput();
		SetAnimation();
		velocity = CalculateMoveVelocity(velocity, speed, direction);
		velocity = GetNode<KinematicBody2D>("../../../Player").MoveAndSlide(velocity, FLOOR_NORMAL);
	}

	public void SetAnimation(){
		if(direction.x < 0){
			GetNode<AnimationPlayer>("../../AnimationPlayer").Play("StanceBack");
		}else if(direction.x > 0){
			GetNode<AnimationPlayer>("../../AnimationPlayer").Play("StanceForward");
		}else{
			GetNode<AnimationPlayer>("../../AnimationPlayer").Play("Idle");
		}
	}
	public override void OnAnimationFinish(string animationName){
		return;
	}
}
}
