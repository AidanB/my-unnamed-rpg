using Godot;
using System;
namespace player{

public class Idle : inputState
{
	public override void Enter(){
		//GD.Print("NOW IDLE");
		speed = new Vector2(0.0F, 1000.0F);
		GetNode<AnimationPlayer>("../../AnimationPlayer").Play("Idle");
	}
	
	public override void Update(float delta){
		SetDirectionInput();
		if(direction.x != 0.0F){
			this.EmitSignal("finished", "running");
		}
		velocity = CalculateMoveVelocity(velocity, speed, direction);
		velocity = GetNode<KinematicBody2D>("../../../Player").MoveAndSlide(velocity, FLOOR_NORMAL);
	}
	public override void OnAnimationFinish(string animationName){
		return;
	}
}
}
