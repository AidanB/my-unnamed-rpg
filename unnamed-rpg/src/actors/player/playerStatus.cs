using Godot;
using System;

public class playerStatus : actorStatus
{
	[Signal]
	public delegate void HealthChanged(int newHealth, int maxHealth);
	[Signal]
	public delegate void OffenseModChanged(int newMod);
	[Signal]
	public delegate void DefenseModChanged(int newMod);
	public override void _Ready(){
		maxHealth = 100;
		health = maxHealth;
	}

	new public void TakeDamage(float amount, String[] Properties){
		base.TakeDamage(amount, Properties);
		GD.Print(health);
		EmitSignal("HealthChanged", health, maxHealth);
	}

	new public void ChangeOffenseMod(float amount){
		base.ChangeOffenseMod(amount);
		EmitSignal("OffenseModChanged",offenseMod);
	}
	new public void ChangeDefenseMod(float amount){
		base.ChangeDefenseMod(amount);
		EmitSignal("DefenseModChanged",offenseMod);
	}
}

