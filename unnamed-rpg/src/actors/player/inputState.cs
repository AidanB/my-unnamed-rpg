using Godot;
using System;

namespace player{
public abstract class inputState : kinematicState
{
	public override void HandleInput(InputEvent e){
		if(e.IsActionPressed("stance_toggle")){
			this.EmitSignal("finished", "stance");
		}
	}

	public void SetDirectionInput(){
		direction.x = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
	}

	new public void EnemyHit(KinematicBody2D body){
		body.GetNode<actorStatus>("Status").TakeDamage(baseDamage*GetNode<playerStatus>("../../PlayerStatus").offenseMod, null);
	}

	public override void WeaponHit(Area2D area){
		return;
	}
}
}
