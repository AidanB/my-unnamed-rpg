using Godot;
using System;

public abstract class actorStatus : Node
{

	public float defenseMod = 1.0F;
	public float offenseMod = 1.0F;
	public int health = 0;

	public int maxHealth;

	public bool deterioriate = true;
	public override void _Process(float delta){
		if(deterioriate){
			if(offenseMod != 1.0F){
				OffenseModDeteriorate();
			}
			if(defenseMod != 1.0F){
				DefenseModDeteriorate();
			}
		}
	}
	public void TakeDamage(float amount, String[] properties){
		health -= (int)(amount/defenseMod);
		health = Math.Max(0, health);
	}
	public void ChangeOffenseMod(float amount){
		offenseMod += amount;
	}
	public void ChangeDefenseMod(float amount){
		defenseMod += amount;
	}

	public void Heal(int amount){
		health += amount;
		health = Math.Min(health, maxHealth);
	}

	public void OffenseModDeteriorate(){
		if(offenseMod > 1.0F){
			ChangeOffenseMod(-0.05F);
		}else{
			ChangeOffenseMod(0.05F);
		}
	}
	public void DefenseModDeteriorate(){
		if(defenseMod > 1.0F){
			ChangeDefenseMod(-0.05F);
		}else{
			ChangeDefenseMod(0.05F);
		}
	}
}
