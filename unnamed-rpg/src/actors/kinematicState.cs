using Godot;
using System;

public abstract class kinematicState : State
{
	public static Vector2 FLOOR_NORMAL = new Vector2(-1.0F, 0.0F);
	public Vector2 speed = new Vector2(0.0F, 1000.0F);
	public Vector2 velocity;
	public float gravity = 3000.0F;
	public Vector2 direction = new Vector2(1.0F, 0.0F);

	//Updates the direction of a sprite by scaling it, given a direction and the path to said sprite
	public void UpdateDirection(Vector2 dir, String sprite)
	{
		if(dir.x > 0){
			GetNode<Sprite>(sprite).Scale = new Vector2(1.0F ,1.0F);
		}else if(dir.x < 0){
			GetNode<Sprite>(sprite).Scale = new Vector2(-1.0F ,1.0F);
		}
	}
	public Vector2 CalculateMoveVelocity(Vector2 linearVelocity, Vector2 speed, Vector2 dir){
		Vector2 newVelocity = linearVelocity;
		newVelocity.x = speed.x * dir.x;
		newVelocity.y += gravity * GetPhysicsProcessDeltaTime();
		if(direction.y == -1.0){
			newVelocity.y = speed.y * dir.y;
		}
		return newVelocity;
	}
	    public int baseDamage = 0;

    public override void OnAnimationFinish(string animationName)
    {
        EmitSignal("finished", "previous");
    }

	public void EnemyHit(KinematicBody2D body){
        return;
	}

    public override void Update(float delta)
    {
        return;
    }

	public abstract void WeaponHit(Area2D area);


}
