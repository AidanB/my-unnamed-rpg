using Godot;
using System;

namespace mudman{
public class Dead : enemyState
{
	public override void Enter(){
		GetNode<MudMan>(".").IsDead();
	}
}
}
