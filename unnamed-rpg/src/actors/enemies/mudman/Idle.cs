using Godot;
using System;
namespace mudman{
public class Idle : enemyState
{
	public override void Enter(){
		Owner.GetNode<AnimationPlayer>("AnimationPlayer").Play("Idle");
		//Owner.GetNode<CollisionShape2D>("aggroRadius/CollisionShape2D").Disabled = false;
	}
	
	private void OnAggroRadiusBodyEntered(Node body){
		if(body.Name == "Player"){
			EmitSignal("finished","aggro");
		}
	}

}
}

