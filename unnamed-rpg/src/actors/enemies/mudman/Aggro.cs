using Godot;
using System;
namespace mudman{
public class Aggro : enemyState
{
	public override void Enter(){
		speed = new Vector2(50.0F, 1000.0F);
		FacePlayer("../../sprite");
		//Owner.GetNode<CollisionShape2D>("aggroRadius/CollisionShape2D").Disabled = true;
		Owner.GetNode<AnimationPlayer>("AnimationPlayer").Play("Walk");
	}
	public override void Update(float delta)
	{
		velocity = CalculateMoveVelocity(velocity, speed, direction);
		velocity = GetNode<KinematicBody2D>("../../../MudMan").MoveAndSlide(velocity, FLOOR_NORMAL);
		if(HorizontalDistFromEnemy() < 75){
			EmitSignal("finished", "attack");
		}
	}
}
}
