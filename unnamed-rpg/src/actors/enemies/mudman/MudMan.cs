using Godot;
using System;
using System.Collections;
using System.Collections.Generic;

namespace mudman{
public class MudMan : KinematicBody2D
{
	static String SPRITE = "sprite";

	private Stack stateStack = new Stack();
	public enemyState curState;

	private Dictionary<String, Node> stateMap;
	public override void _Ready()
	{
		stateMap = new Dictionary<String, Node>{
		{"idle", GetNode("States/Idle")},
		{"aggro", GetNode("States/Aggro")},
		{"attack", GetNode("States/Attack")},
		{"stagger", GetNode("States/Stagger")},
		{"dead", GetNode("States/Dead")}
	};
		foreach(Node stateNode in GetNode<Node>("States").GetChildren()){
			stateNode.Connect("finished", this, "ChangeState");
		}
		
		stateStack.Push(GetNode("States/Idle"));
		GetNode("Status").Connect("HealthChanged", this, "OnHealthChanged");
		curState = (enemyState)stateStack.Peek();
		ChangeState("idle");
	}

	public override void _PhysicsProcess(float delta){
		curState.Update(delta);
	}
	private void ChangeState(String stateName){
		curState.Exit();

		if(stateName == "previous"){
			stateStack.Pop();
		}else if(new List<String>{"stagger", "attack"}.Contains(stateName)){
			stateStack.Push(stateMap[stateName]);
		}else{
			Node newState = stateMap[stateName];
			stateStack.Pop();
			stateStack.Push(newState);
		}
		curState = (enemyState)stateStack.Peek();

		if(stateName != "previous"){
			curState.Enter();
		}

	}

	/*public void TakeDamage(float amount, String[] properties){
		GetNode<playerStatus>("PlayerStatus").TakeDamage(amount, properties);
	}*/

	public void OnHealthChanged(){
		if(GetNode<actorStatus>("Status").health == 0){
			ChangeState("dead");
		}
	}

	public void IsDead(){
		SetPhysicsProcess(false);
		QueueFree();
	}

	private void OnAnimationFinished(String anim_name){
		curState.OnAnimationFinish(anim_name);
	}
	private void OnWeaponBodyEntered(KinematicBody2D body){
		GD.Print(body.Name);
		if(body.Name != "Player"){
			return;
		}
		curState.EnemyHit(body);
	}
}
}


