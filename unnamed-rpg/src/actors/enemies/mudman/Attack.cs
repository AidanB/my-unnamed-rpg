using Godot;
using System;
namespace mudman{
public class Attack : enemyState
{
	public override void Enter(){
		baseDamage = 20;
		speed = new Vector2(0.0F, 0.0F);
		Owner.GetNode<AnimationPlayer>("AnimationPlayer").Play("Attack");
	}
}
}
