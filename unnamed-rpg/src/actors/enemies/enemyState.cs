using Godot;
using System;

namespace mudman{
public abstract class enemyState : kinematicState
{
	public void FacePlayer(String sprite){
		if(GetNode<KinematicBody2D>("../../../Player").GlobalPosition.x > GetNode<KinematicBody2D>("../../../MudMan").GlobalPosition.x){
			direction = new Vector2(1.0F, 0.0F);
		}else{
			direction = new Vector2(-1.0F, 0.0F);
		}
		UpdateDirection(direction, sprite);

	}
	public float HorizontalDistFromEnemy(){
		return Math.Abs(GetNode<KinematicBody2D>("../../../Player").GlobalPosition.x - GetNode<KinematicBody2D>("../../../MudMan").GlobalPosition.x);
	}

	new public void EnemyHit(KinematicBody2D body){
		body.GetNode<playerStatus>("PlayerStatus").TakeDamage(baseDamage*GetNode<actorStatus>("../../Status").offenseMod, null);
	}
	public override void HandleInput(InputEvent e){
		return;
	}
	public override void WeaponHit(Area2D area){
		return;
	}
}
}
